# README #

This contains a version of the displaCy visualiser used in PDGN

### What it does ###

displaCy takes text, sends it to a parsing service, then displays the results visually using SVG. This extension provides a layer where you can see (and alter) the parse results, compare against others, and distort sentences using a distorter service.

### Setup ###

The files were written to be served by [harp server](http://harpjs.com/), but I am using a compiled set of HTML files in production.

```
npm install -g harp
harp compile . html/
```
this installs harpjs (globally) then compiles the scss and jade files to the html directory