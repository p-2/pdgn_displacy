/**
 * Display functions
 */

var pdgn = (function($){

    /* page header */
    var header = function(title){
        return $('<header/>', {'class': 'pdgn-header'}).append($('<h2/>').text(title));
    },

    /**
     * main form containing details of the parsed structure of a text
     * allows the user to change the way the text has been parsed and 
     * alter words or word order
     */
    parseForm = function(parsed, cb) {
    	if ( parsed && parsed.words ) {
	        var f = $('<form/>', {'class':'parseform'}),
                wc = $('<div/>',{'class':'words-container'}).append($('<h3/>', {'class':'expando'}).text('Words')),
                ac = $('<div/>',{'class':'arcs-container'}).append($('<h3/>', {'class':'expando'}).text('Relationships')),
                tb = $('<div/>', {'class':'pdgnButtons'}),
                wlst = $('<div/>', {'class':'wordList'}),
                alst = $('<div/>', {'class':'arcList'}),
	            wrds = [], wrdord = [], i;

	        for(i = 0; i < parsed.words.length; i++) {
	            wrds.push(parsed.words[i].text);
                wrdord.push(i);
	            wlst.append(selectList("word-"+i, pdgn_data.word_tags, parsed.words[i].tag, parsed.words[i].text, 'wordItem'));
	        }
            wc.append(wlst);
	        for(var j = 0; j < parsed.arcs.length; j++) {
	            alst.append(arc("arc-"+j, parsed.arcs[j], wrds, pdgn_data.arc_tags));
	        }
            ac.append(alst);
            tb.append(
                button("pdgn-reload", "Reload"),
                button('pdgn-update', 'Update display'),
                button("pdgn-save", "Save Changes"),
                $('<input/>', {'type':'hidden','id':'wordorder','value':wrdord.join(':')}),
                $('<input/>', {'type':'hidden','id':'pdgntext','value':parsed.text})
            );
            f.append(wc, ac, tb);
	        return f;
	    }
        if ( cb ) {
            cb();
        }
    },

    /**
     * displays a list of transformations applied to a given text
     */
    textHistory = function(parsed) {
        var c = $('<div/>', {'id':'transformations'}),
            lst, i, actions,
            tfmLength = parsed.transformations.length;
        if (tfmLength) {
            c.append(h('Distortions applied to this text'));
            lst = $('<ul/>', {'data-parent-textid': parsed._id});
            listTexts(parsed.transformations, lst, 'transform');
            c.append(lst);
        }
        return c;
    },

    /**
     * displays a list of texts retrieved from the database from previous sessions
     */
    listTexts = function(texts, list, cssClass)
    {
        var textsLength = texts.length,
            cls = cssClass? ' '+cssClass: '',
            i, textlink, dellink, loadmore;
        if (textsLength) {
           for (i = 0; i < textsLength; i++) {
                textlink = $('<a/>', {
                    'class':'textlink'+cls,
                    'href':'#',
                    'data-textid': texts[i]._id,
                    'data-created_at': texts[i].created_at
                }).text(texts[i].text);
                dellink = $('<a/>', {
                    'class':'dellink ui-icon ui-icon-delete'+cls,
                    'href':'#',
                    'data-textid': texts[i]._id
                }).text('delete');
                list.append($('<li/>').append(textlink, dellink));
            }
        }
    },

    /**
     * adds a button to load more entries from the database
     */
    loadMoreButton = function(){
        return $('<a/>', {
            'class':'button loadmore',
            'role': 'button',
            'href':'#',
        }).text('load more');
    },


    /**
     * creates a button
     * @param string name of button (used in ID attribute)
     * @param string label for button (displayed as text on the button)
     */
    button = function(name, label) {
        return $('<a/>', {
            'class':'button',
            'role': 'button',
            'aria-label': label,
            'href':'#',
            'id': name
        }).text(label);
    },

    /**
     * creates a select dropdown
     * @param string name of the select element (used in ID attribute)
     * @param array information to make option elements for the list. Each element should be an object with key:label structure
     * @param string selected option key
     * @param string text for label elem,ent to wrap around the select
     * @param string class names for select element
     */
    selectList = function(name, options, selectedOption, label, className) {
        var sel = $('<select/>').attr('name', name).attr('id', name),
            opt, o, l, container;
        for(opt in options) {
            o = $('<option/>').attr('value', opt);
            if ( opt == selectedOption ) {
                o.attr('selected', 'selected');
            }
            //l = prepend ? opt+': '+opts[opt]: opts[opt];
            o.text( options[opt] );
            sel.append(o);
        }
        if ( label && label !== '' ) {
            container = $('<div/>');
            if ( className ) {
                container.addClass(className);
            }
            return container.append(span('label', label), sel);
        } else {
            if ( className ) {
                sel.addClass(className);
            }
            return sel;
        }
    },

    // adds a set of words parsed in the past to the similar words list
    li = function(w) {
        return $('li').text(w.text);
    },

    h = function(text, level) {
        if ( !level ) {
            level = 3;
        }
        return $('<h'+level+'/>').text(text);
    },

    // constructs DOM fragment for arc
    arc = function(name, arc, words, opts) {
        return $('<div class="arc"/>').append(
            selectList(name+'-start', words, arc.start, false, 'wordselect start'),
            selectList(name, opts, arc.label, false, 'arclabel'),
            selectList(name+'-end', words, arc.end, false, 'wordselect end'),
            selectList(name+'-dir', {'left':'Left','right':'Right'}, arc.dir, false, 'dir')
        );
    },

    // creates radio inputs
    radio = function(name, opts, sel) {
        var s = $('<span/>'),
            i, l, r;
        for (i = 0; i < opts.length; i++){
            l = $('<label/>').text(opts[i]).attr('for', name+'-'+i);
            r = $('<input/>')
                .attr('type', 'radio')
                .attr('name', name)
                .attr('id', name+'-'+i)
                .attr('value', opts[i]);
            if ( sel == opts[i] ) {
                r.attr('checked', 'checked');
            }
            s.append(l.append(r));
        }
        return s;
    },

    // creates a <span> element and return it
    span = function(cls, text) {
        var s = $('<span/>').text(text);
        if ( cls ) {
            s.addClass(cls);
        }
        return s;
    };

    return {
        header: header,
        parseForm: parseForm,
        textHistory: textHistory,
        listTexts: listTexts,
        loadMore: loadMoreButton,
        button: button,
        selectList: selectList,
        li: li,
        h: h,
        arc: arc,
        radio: radio,
        span: span
    };
})(jQuery);