//- ----------------------------------
//- PDGN data storage
//- ----------------------------------

var pdgndb = (function($) {

    var cache = {},
    current = {},

    /**
     * stores current parse
     */
    setCurrent = function(data){
        current = data;
    },

    /**
     * gets currrent parse
     */
    getCurrent = function() {
        return current;
    },

    /**
     * stores a parsed sentence
     * @param json the results from the spacy parser
     * @param text sent to the parser
     * @param callback function
     */
    storeParse = function(json, text, cb){
        $.ajax({
        	url: dataAPI+'text/',
            method: 'POST',
            contentType: 'application/json',
            //processData: false,
            data: prepareParseObj(json, text)
        }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 400 ) {
                // error storing text
                cb( false, textStatus, data );
                console.log("Mongo/Mongoose error", data);
            } else if ( jqXHR.status === 201 ) {
                cacheData(data);
                cb( data, textStatus, jqXHR );
            } else {
                console.log("Unknown status returned: "+jqXHR.status);
                console.log(data);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, errorThrown );
        });
    },

    storeTransform = function(doc_id, transformObj, cb){
        $.ajax({
            url: dataAPI+'text/'+doc_id+'/transformations',
            method: 'POST',
            contentType: 'application/json',
            //processData: false,
            data: transformObj
        }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 400 ) {
                // error storing text
                cb( false, textStatus, data );
                console.log("Mongo/Mongoose error", data);
            } else if ( jqXHR.status === 404 ) {
                // error fetching parent document
                cb( false, status, data );
            } else if ( jqXHR.status === 201 ) {
                cacheData(data);
                cb( data, textStatus, jqXHR );
            } else {
                console.log("Unknown status returned: "+jqXHR.status);
                console.log(data);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, errorThrown );
        });
    },

    /**
     * gets data from storage
     */
    getData = function(cb) {
        $.ajax({
            method: "GET",
            url: dataAPI+'text',
            dataType: 'json'
        }).done( function( data, textStatus, jqXHR ) {
            if ( jqXHR.status === 404 ) {
                // no text found
                if ( data.message ) {
                    cb(false, textStatus, data.message);
                } else {
                    console.log("Mongo/Mongoose error", data);
                }
            } else if ( jqXHR.status === 200 ) {
                cacheData(data);
                cb( data, textStatus, jqXHR );
            } else {
                console.log("Unknown status returned: "+jqXHR.status);
                console.log(data);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            cb( false, textStatus, errorThrown );
        });
    },

    /**
     * gets data from storage which has been added after a given date
     */
    getDataAfter = function(dateafter, cb) {
        $.ajax({
            method: "GET",
            url: dataAPI+'text/from/'+encodeURIComponent(dateafter),
            dataType: 'json',
        }).done( function( data, textStatus, jqXHR ) {
            if ( jqXHR.status === 404 ) {
                // no text found
                if ( data.message ) {
                    cb(false, textStatus, data.message);
                } else {
                    console.log("Mongo/Mongoose error", data);
                }
            } else if ( jqXHR.status === 200 ) {
                cacheData(data);
                cb( data, textStatus, jqXHR );
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            cb( false, textStatus, errorThrown );
        });
    },

    /**
     * gets data from storage for all sentences which have the 
     * same original text input
     */
    getDataByText = function(text, cb) {
        var cached = getDataFromCache({'text':text});
        if ( cached ) {
            cb(cached, 'cached', false);
        } else {
            $.ajax({
                method: "GET",
                url: dataAPI+'text/str/'+encodeURIComponent(text),
                dataType:'json'
            }).done(function( data, textStatus, jqXHR ){
                if ( jqXHR.status === 404 ) {
                    // no text found
                    if ( data.message ) {
                        cb(false, textStatus, data.message);
                    } else {
                        console.log("Mongo/Mongoose error", data);
                    }
                } else if ( jqXHR.status === 200 ) {
                    cb(data, textStatus, xhr, text);
                }
            }).fail(function( jqXHR, textStatus, errorThrown ){
                cb( false, textStatus, errorThrown );
            });
        }
    },

    /**
     * gets data from storage for all sentences which have the 
     * same parse "signature"
     */
    getDataBySignature = function(signature, cb) {
        $.ajax({
            method: "GET",
            url: dataAPI+'text/sig/'+encodeURIComponent(signature),
            dataType:'json'
       }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 404 ) {
                // no text found
                if ( data.message ) {
                    cb(false, textStatus, data.message);
                } else {
                    console.log("Mongo/Mongoose error", data);
                }
            } else if ( jqXHR.status === 200 ) {
                cb(data, textStatus, xhr, text);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, errorThrown );
        });
    },

    /**
     * gets data from storage for all sentences which have the 
     * same parse "signature"
     */
    getDataByWordTags = function(wordtags, cb) {
        $.ajax({
            method: "GET",
            url: dataAPI+'text/tags/'+encodeURIComponent(wordtags),
            dataType:'json'
        }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 404 ) {
                // no text found
                if ( data.message ) {
                    cb(false, textStatus, data.message);
                } else {
                    console.log("Mongo/Mongoose error", data);
                }
            } else if ( jqXHR.status === 200 ) {
                cb(data, textStatus, xhr, text);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, errorThrown );
        });
    },

    /**
     * gets data for a text id
     */
    getDataByID = function(id, cb) {
        var cached = getDataFromCache({'id':id});
        if ( cached ) {
            cb(cached, 'cached', false);
        } else {
            $.ajax({
                method: "GET",
                url: dataAPI+'text/'+encodeURIComponent(id),
                dataType:'json'
            }).done(function( data, textStatus, jqXHR ){
                if ( jqXHR.status === 404 ) {
                    // no text found
                    if ( data.message ) {
                        cb(false, textStatus, data.message);
                    } else {
                        console.log("Mongo/Mongoose error", data);
                    }
                } else if ( jqXHR.status === 200 ) {
                    cb(data, textStatus, xhr, text);
                }
            }).fail(function( jqXHR, textStatus, errorThrown ){
                cb( false, textStatus, errorThrown );
            });
        }
    },

    /**
     * deletes data from storage
     */
    deleteData = function(textid, transformid, cb) {
        deleteDataFromCache(textid, transformid);
        var path = (transformid)? 'text/'+textid+'/transformations/'+transformid: 'text/'+textid;
        $.ajax({
            method: "DELETE",
            url: dataAPI+path,
            dataType: 'json'
        }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 404 || jqXHR.status === 400 ) {
                // no text found or other error
                if ( data.message ) {
                    cb(false, textStatus, data.message);
                } else {
                    cb(false, textStatus, data);
                    console.log("Mongo/Mongoose error", data);
                }
            } else if ( jqXHR.status === 204 ) {
                cb(data, textStatus, jqXHR);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, errorThrown );
        });
    },

    /**
     * stores retrieved data in a local cache
     */
    cacheData = function( data )
    {
        for (var i = 0; i < data.length; i++ ) {
            cache[data[i]._id] = data[i];
        }
    },

    /**
     * retrieves data from the cache
     */
    getDataFromCache = function( query )
    {
       if (cache) {
           for (var id in cache) {
                if ( query.id && query.id === id ) {
                    return cache[id];
                }
                if ( query.text && query.text === cache[id].text ) {
                    return cache[id];
                }
            }
        }
        return false;
    },

    /**
     * deletes data from the cache
     */
    deleteDataFromCache = function(textid, transformid)
    {
        /* first see if we have cached data */        
        var cached = getDataFromCache({'id':textid});
        if ( cached ) {
            /* if one of the distortions is being deleted */
            if ( transformid ) {
                /* remove the distortion from our cached copy */
                var todelete = false;
                for ( var i = 0; i < cached.transformations.length; i++ ) {
                    if ( cached.transformations.id === transformid ) {
                        todelete = i;
                    }
                }
                if ( todelete !== false ) {
                    for (id in cache) {
                        if ( id === textid ) {
                            cache[textid].transformations.splice(todelete,1);
                        }
                    }
                }
            } else {
                /* delete entry */
                delete cache[textid];
            }
        }
    },

    /**
     * prepares a data object to store a newly parsed string
     * parses the JSON and adds members to the resulting object
     */
    prepareParseObj = function(json, text) {
        var parsed = json;
        parsed.signature = getParseSignature(parsed);
        parsed.wordtags = getWordSignature(parsed);
        parsed.text = text;
        parsed.transformations = [];
        return JSON.stringify(parsed);
    },

    /**
     * gets the "signature" of a parsed sentence
     * This consists of the word tags, arc labels and directions
     * placed in a colon-separated string
     */
    getParseSignature = function(parseResult) {
        // signatures are just the word tags and the arc labels
        let sig = [];
        for(var i = 0; i < parseResult.words.length; i++) {
            sig.push(parseResult.words[i].tag);
        }
        for(var j = 0; j < parseResult.arcs.length; j++) {
            sig.push(parseResult.arcs[j].label);
        }
        return sig.join(':');
    },

    /**
     * gets the word "signature" of a parsed sentence
     * This consists of the word tags sorted and placed in a colon-separated string
     */
    getWordSignature = function(parseResult) {
        // signatures are just the word tags and the arc labels
        let sig = [];
        for(var i = 0; i < parseResult.words.length; i++) {
            sig.push(parseResult.words[i].tag);
        }
        return sig.sort().join(':');
    };

    return {
        getCurrent: getCurrent,
        setCurrent: setCurrent,
        save: storeParse,
        storeTransform: storeTransform,
        get: getData,
        after: getDataAfter,
        bytext: getDataByText,
        bysig: getDataBySignature,
        bytags: getDataByWordTags,
        byid: getDataByID,
        delete: deleteData,
        getParseSignature: getParseSignature,
        getWordSignature: getWordSignature
    };
})(jQuery);
