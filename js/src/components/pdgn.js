//- ----------------------------------
//- PDGN
//- ----------------------------------

(function($){
    var defaultText = 'Hello world',
    defaultModel = 'en',
    parsedText = [],
    displacy,
    errorto,
    storedTexts = [],
    loading = function(){
        $('body').toggleClass('loading');
    },

    showError = function(err) {
        $('#error').html(err).show();
        window.clearTimeout(errorto);
        errorto = window.setTimeout(function(){
        	$('#error').fadeOut();
        }, 5000);
    }, 

    storeParse = function(json, text) {
        pdgndb.save(json, text, function(data, status, error) {
            if ( data ) {
                showPDGN(data);
                loading();
            } else {
                showError(error);
            }
        });
    },

    showPDGN = function(data) {
        $('#pdgn').empty();
        $('#pdgn').append(pdgn.header('Parse Results'));
        $('#pdgn').append(pdgn.parseForm(data));
        $('#pdgn').append(pdgn.textHistory(data));
        $('#pdgn').show();
        activateWordSort();
    },

    updateParseForm = function(data) {
        $('.parseform').replaceWith(pdgn.parseForm(data));
        activateWordSort();
    },

    activateWordSort = function() {
        try {
            $('.wordList').sortable("destroy");
        } catch(e) {};
        $('.wordItem').each(function(idx, val){
            $(this).data('wordindex', $(this).find('select').attr('id').replace('word-', ''));
            $(this).append(pdgn.span('wlhandle ui-icon ui-icon-arrowthick-2-n-s'));
        });
        $('.wordList').sortable({
            'handle':'.wlhandle',
            'axis':'y',
            'update': updateWordOrder
        });
        updateWordOrder();
    },

    // runs the displacy visualiser
    run = function(){
        var text = $('#parseinput').val();
        if ( text !== '' ) {
        	loading();
        	pdgndb.bytext(text, function(data, status, xhr, errorText){
                if ( false === data ) {
      				parseText(text, function(data, status){
       					if ( false !== data ) {
                            console.log('Got data from parser - nothing found in database for '+text);
    						console.log(data);
                            pdgndb.setCurrent(data);
        					displacy.render(data);
	    					pdgndb.save(data, text, function(data, status) {
    							showPDGN(data);
    							loading();
    						});
    					} else {
    						loading();
    					}
       				});
    			} else {
                    console.log('Got data from database');
                    console.log(data);
                    pdgndb.setCurrent(data);
    				displacy.render(data);
    				showPDGN(data);
    				loading();
    			}
        	});
        } else {
        	showError('Please enter some text');
        }
    },

    parseText = function(text, cb)
    {
        $.ajax({
        	url: spacyAPI,
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify({
            	'text':text,
            	'model':'en',
            	'collapse_punctuation':1,
            	'collapse_phrases':1
            })
        }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 500 ) {
                // parser error
                cb(false, textStatus, "spaCy parser error");
            } else if ( jqXHR.status === 200 ) {
                cb(data, textStatus, jqXHR);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, "Error connecting to spaCy service");
        });

    },

    // Update View
    updateView = function(text) {
        $('#parseinput').value = text;
    },


    // takes values from the form and updates the displacy interface
    updatePDGN = function(ev) {
        var parsed = pdgndb.getCurrent(),
            transformObj = {'words':[],'arcs':[], 'actions':[]},
            wo = $.map($('#wordorder').val().split(':'), function(value){return parseInt(value, 10);}),
            neworder = false,
            hasChanged = false,
            words = [],
            i, idx, j;
        ev.preventDefault();
        console.log(parsed);
        // first see if the word order has changed
        for(i = 0; i < parsed.words.length; i++) {
            if (wo[i] !== i) {
                neworder = true;
                transformObj.actions.push('wordorder');
            }
        }
        // populate words
        for(i = 0; i < wo.length; i++) {
            transformObj.words[i] = {};
            transformObj.words[i].text = $('#word-'+wo[i]).parent().find('.label').text();
            transformObj.words[i].tag = $('#word-'+wo[i]).val();
            if ( transformObj.words[i].tag != parsed.words[wo[i]].tag ) {
                hasChanged = true;
                transformObj.actions.push('wordtags');
            }
        }
        for(i = 0; i < wo.length; i++) {
            words.push(parsed.words[wo[i]].text);
        }
        transformObj.text = words.join(' ');
        transformObj.wordorder = wo.join(':');
        // populate arcs
        for(j = 0; j < parsed.arcs.length; j++) {
            transformObj.arcs[j] = {};
            transformObj.arcs[j].label = $('#arc-'+j).val();
            transformObj.arcs[j].dir = $('#arc-'+j+'-dir').val();
            transformObj.arcs[j].start = parseInt($('#arc-'+j+'-start').val());
            transformObj.arcs[j].end = parseInt($('#arc-'+j+'-end').val());
            if (transformObj.arcs[j].label != parsed.arcs[j].label ||
                transformObj.arcs[j].dir != parsed.arcs[j].dir ||
                transformObj.arcs[j].start != parsed.arcs[j].start ||
                transformObj.arcs[j].end != parsed.arcs[j].end) {
                hasChanged = true;
                transformObj.actions.push('wordrelationships');
            }
        }

        switch (ev.target.getAttribute('id')) {
            case 'pdgn-reload':
                displacy.render(parsed);
                showPDGN(parsed);
                break;
            case 'pdgn-update':
                console.log('Updating displacy', transformObj);
                displacy.render(transformObj);
                break;
            case 'pdgn-save':
                if ( hasChanged || neworder ) {
                    transformObj.signature = pdgndb.getParseSignature(transformObj);
                    transformObj.wordtags = pdgndb.getWordSignature(transformObj);
                    pdgndb.storeTransform(parsed._id, JSON.stringify(transformObj), function(data, status){
                        if ( data ) {
                            displacy.render(transformObj);
                            showPDGN(data);
                        }
                    });
                } else {
                    showError('Nothing to save');
                }
                break;
        }
    },

    getIndex = function(value, arr) {
        var idx = $.inArray(value, arr);
        if ( idx !== -1 ) {
            return idx;
        } else {
            return false;
        }
    },

    loadMore = function(cb) {
        var last = new Date($('#history-list .textlink:last').data('created_at'));

        console.log(last);
        pdgndb.after(last, function(data, status, xhr, errorText){
            if ( false !== data ) {
                console.log(data);
            }
            cb(data, status, xhr, errorText);
        });
    },

    distort = function() {
        $.ajax({
            url: distorterAPI,
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(pdgndb.getCurrent())
        }).done(function( data, textStatus, jqXHR ){
            if ( jqXHR.status === 500 ) {
                // parser error
                cb(false, textStatus, "distorter error");
            } else if ( jqXHR.status === 200 ) {
                cb(data, textStatus, jqXHR);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ){
            cb( false, textStatus, "Error connecting to distorter service");
        });
    },

    // Update URL
    updateURL = function(text, model, settings) {
        var url = [
            'text=' + encodeURIComponent(text),
            'model=' + encodeURIComponent(model),
            'cpu=' + (settings.collapsePunct ? 1 : 0),
            'cph=' + (settings.collapsePhrase ? 1 : 0)
        ];

        history.pushState({ text, model, settings }, null, '?' + url.join('&'));
    },

    // Get URL Query Variables

    getQueryVar = function(key) {
       var query = window.location.search.substring(1);
       var params = query.split('&').map(param => param.split('='));

       for(var param of params) if(param[0] == key) return decodeURIComponent(param[1]);
       return false;
    },

    updateWordOrder = function(){
        var wordorderarr = [];
        $('.wordItem').each(function(){
            wordorderarr.push($(this).data('wordindex'))
        });
        $('#wordorder').val(wordorderarr.join(':'));
        console.log($('#wordorder').val());
    };

    // UI
    $(document).ready(function(){
        displacy = new displaCy(spacyAPI, {
            container: '#displacy',
            engine: 'spacy',
            defaultText: defaultText,
            defaultModel: defaultModel,
            collapsePunct: true,
            collapsePhrase: true,
            distance: 200,
            offsetX: 150,
            arrowSpacing: 10,
            arrowWidth: 8,
            arrowStroke: 2,
            wordSpacing: 40,
            onStart: loading,
            onSuccess: storeParse
        });

        /*svgPanZoom('#displacy', {
          zoomEnabled: true,
          controlIconsEnabled: true,
          fit: true,
          center: true,
        });*/
        // UI Event Listeners
        $('#submit').on('click', run);
        $('#parseinput').on('keydown', function(e) {
            if ( e.keyCode == 13 ) {
                run();
            }
        });
        // load text history
        pdgndb.get(function(texts, status){
            if (texts) {
                $('#pdgn-history').prepend($('<h2/>').text('Previous texts'));
                pdgn.listTexts(texts, $('#history-list'));
                $('#pdgn-history').append(pdgn.loadMore());
            }
        });
        $('#pdgn-history').on('click', '.loadmore', function(e){
            e.preventDefault();
            loadMore(function(data, status, xhr, errorText){
                if (false === data || !data.length) {
                    $('.loadmore').remove();
                    $('#pdgn-history').append($('<p/>', {'class':'nomore'}).text('No more text to display'));
                    setTimeout(function(){
                        $('.nomore').fadeOut();
                    }, 2000);
                } else {
                    pdgn.listTexts(data, $('#history-list'));
                }
            });
        });
        $('#pdgn-history').on('click', '.textlink', function(e){
            e.preventDefault();
            var textid = $(this).data('textid');
            console.log(textid);
            pdgndb.byid(textid, function(data, status) {
                if ( data ) {
                    pdgndb.setCurrent(data);
                    displacy.render(data);
                    showPDGN(data);
                }
            });
        });
        $('#pdgn-history').on('click', '.dellink', function(e){
            e.preventDefault();
            var textid = $(this).data('textid');
            console.log(textid);
            if (confirm("Are you sure?")) {
                pdgndb.delete(textid, false, function(data, status, msg){
                    if ( false === data ) {
                        console.log("Failed to delete text", msg);
                    } else {
                        $('#pdgn-history [data-textid="'+textid+'"]').parent().remove();
                    }
                });
            }
        });
        $('#pdgn').on('click', '.textlink', function(e){
            e.preventDefault();
            var textid = $(this).closest('ul').data('parent-textid'),
                transformid = $(this).data('textid');
            console.log(textid, transformid);
            pdgndb.byid(textid, function(data, status) {
                if ( data && data.transformations.length ) {
                    for(var t = 0; t < data.transformations.length; t++) {
                        if (data.transformations[t]._id === transformid) {
                            displacy.render(data.transformations[t]);
                            updateParseForm(data.transformations[t]);
                        }
                    }
                }
            });
        });
        $('#pdgn').on('click', '.dellink', function(e){
            e.preventDefault();
            var textid = $(this).closest('ul').data('parent-textid'),
                transformid = $(this).data('textid');
            console.log(textid, transformid);
            if (confirm("Are you sure?")) {
                pdgndb.delete(textid, transformid, function(data, status, msg){
                    if ( false === data ) {
                        console.log("Failed to delete text", msg);
                    } else {
                        if ($(this).closest('ul').children().length === 1) {
                            $('#text-history').remove();
                        } else {
                            //$('#pdgn [data-textid="'+transformid+'"]').parent().remove();
                            displacy.render(pdgndb.getCurrent());
                            showPDGN(pdgndb.getCurrent());
                        }
                    }
                });
            }
        });
        $('#pdgn').on('click', '.button', function(e){
        	updatePDGN(e);
        });
        /*var text = getQueryVar('text') || getQueryVar('full') || getQueryVar('manual') || getQueryVar('steps') || defaultText;
        var args = [ text, defaultModel ];
        if(getQueryVar('text')) updateView(...args);
        if(getQueryVar('full') || getQueryVar('manual') || getQueryVar('steps')) updateURL(...args);
        displacy.parse(...args);*/
    });

})(jQuery);
