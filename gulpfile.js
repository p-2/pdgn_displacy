// Gulp
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleancss = require('gulp-cleancss'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel');

gulp.task('lint', function() {
  return gulp.src('./js/src/components*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

/* minify javascript */
gulp.task('compressjs', function(cb){
    pump([
        gulp.src([
            './js/src/components/displacy.js',
            './js/src/components/storage.js',
            './js/src/components/data.js',
            './js/src/components/display.js',
            './js/src/components/pdgn.js',
        ]),
        sourcemaps.init(),
        babel({presets: ['es2015']}),
        concat('scripts.js'),
        uglify(),
        sourcemaps.write('./maps'),
        gulp.dest('./js')
    ],
    cb
    );
});


/* compile css */
gulp.task('compilecss', function() {
    return gulp.src('scss/*.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(gulp.dest('./css'));
});

/* minify sass */
gulp.task('compresscss', function() {
    return gulp.src('css/style.css')
        .pipe(cleancss())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./css'));
});


/* Watch Files For Changes */
gulp.task('watch', function() {
    gulp.watch('scss/**/*.scss', ['css']);
    gulp.watch('js/src/components/*.js', ['js']);
});

/* copy vendor files */
gulp.task('copyvendor', function() {
    return gulp.src('bower_components/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('js/vendor'));
})

// Default Task
gulp.task('default', ['watch']);
gulp.task('js', ['compressjs']);
gulp.task('css', ['compilecss', 'compresscss'])