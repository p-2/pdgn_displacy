Interface
---------

### Text input

This will allow the user to enter text to be parsed by spaCy

### Displacy visualiser

Any parsing results or manual changes / distortions will be displayed here by the displact visualiser

### Form to manually trasform text

A manual interface to enable the transformation of text (re-tagging / reordering / re-definition of relationships)

### Form to apply distortions

A form to allow the manual selection of various distorters to apply to text input

### Related items

A list of other pieces of text which are structured in a similar way to the current result. This could be based on the full parsed structure of the text (word tags and relationships), just word tags, or text with similar types of words.

### History

A depiction of the parsed text, and any transformations which have been applied to it.


Database Schema
---------------

Each text will need to be stored as the original result from the spaCy parser along with any transformations which have been applied to the text using the interface.

The storage format will include the raw parse result, and some additional properties for signatures depicting the text structure:

 * text: the original text
 * words: the words from the text, their word tags and word order
 * arcs: relationships between words and the direction of these relationships
 * signature: a string representing the text as word tags and arcs (i.e. the parsed text structure, not the words of the sentence)
 * wordtags: a string representing the different types of words present in the text, i.e. NN:JJ:VB (a noun, adjective and a verb)
 * wordtagsaz: a string representing the different types of words present in the text, with the tags in alphabetical order (not the order they appear in the text, so for the previous example, this would be JJ:NN:VB)
 * transformations: a collection of actions which have been applied to the text

Transformations will have the same structure as a parse result (text, words, arcs, signature and wordtags) but will have an additional property to describe what type of transformation has been carried out.

"signatures" could be used to identify text in the database which has the same structure (word tags and relationships between words), "wordtags" could be used to identify text in the database which has as similar word structure, and "wordtagsaz" to identify text in the database which has similar words (but not in the same order). 

Workflow
--------

When a piece of text is submitted to the spaCy parser using the form, there are two possible outcomes depending on whether the text is already present in the database:

1. If the text is present, it is loaded into the interface with all the transformations which have been applied to it.
2. If the text is not present, it is parsed, and the results saved.

The text can be re-ordered, re-tagged, or sent to the distorter service to be manipulated in other ways. The results of each action can be viewed in the visualiser, or saved as a transformation.

The history section of the interface will show a list of texts which have been worked on (most recent first) which can be clicked on to load them into the interface.

The similar text section will show other texts which have similar word types, or a similar syntactic structure, and allow you to apply transformations which have been saved when these texts have been manipulated. Similarities in texts will be determined by the words, "signature" or "wordtags" of the texts.

Integration of distorters
-------------------------

Ability to select a distorter to act on text input and display the results of the distortion below in the visualizer. If this is incorrect, provide the ability to re-tag/re-define the relationships of the original parsed text, and re-apply the distortion.

Enter text, choose distortion
parse/tag -> send to distorter -> visualize results
present new, distorted sentence under the original input?

Some distorters will replace parts of the original text (straightforward single word replacements or replacements of words with phrases/phrases with words)

Dictionary integration
----------------------

Present options to use a dictionary to look up words to inform substitutions
- get tense information
- false friends

Multiple users
--------------

By default the interface will present all texts stored in the database by any user. It should be possible to assign operations to users so the history section only includes the texts a given user has been working on.